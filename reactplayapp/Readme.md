This is an application that allows companies and organizations to allow their employees to set vacations before they take their time off. 

# Server Side of this project
This is the UI side of things. In order to use this UI you will need to setup it's server side which can be found on this Repo

# Technology Stack
* React
* Redux
* React Router
* Redux Saga

# Usage
Run the folling commands
npm install / yarn depending on what have setup on machine (installs dependencies)
npm start / yarn startdepending on what have setup on machine (Starts the UI App)
